import numpy as np
import pandas as pd
import os
import time
import pickle
import sys

if __name__ == '__main__':
    metric = sys.argv[1]
    dir = '../results/' + metric

    fnames = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
    res = [np.load('{}/{}'.format(dir, fname)) for fname in fnames]

    for f, a in zip(fnames, res):
        print(f)
        #print(a)
        print(np.mean(a, axis=0))
        print(np.std(a, axis=0))