from keras.layers.core import Lambda
from keras.models import Sequential, Model
from tensorflow.python.framework import ops
import keras.backend as K
import tensorflow as tf
from tensorflow.keras import datasets
import numpy as np
import pandas as pd
import keras
import cv2
import matplotlib as mlp
from matplotlib import pyplot as plt
import pickle
import os
import random
import time
from skimage.metrics import structural_similarity as ssim

from keras_explain.grad_cam import GradCam

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.7
sess = tf.Session(config=config)
K.set_session(sess)

from saliency_metrics import similarity, cc, kldiv, cosine, standardize_map

DIR = '../model'

def target_category_loss(x, category_index, nb_classes):
    return tf.multiply(x, K.one_hot([category_index], nb_classes))

def target_category_loss_output_shape(input_shape):
    return input_shape

def normalize(x):
    # utility function to normalize a tensor by its L2 norm
    return x / (K.sqrt(K.mean(K.square(x))) + 1e-5)

def _compute_gradients(tensor, var_list):
    grads = tf.gradients(tensor, var_list)
    return [grad if grad is not None else tf.zeros_like(var) for var, grad in zip(var_list, grads)]

def get_gradient(input_model, image, category_index, layer_name):
    nb_classes = 10
    category_index = K.placeholder(shape=(), dtype=tf.int64)

    target_layer = lambda x: target_category_loss(x, category_index, nb_classes)
    x = Lambda(target_layer, output_shape = target_category_loss_output_shape)(input_model.output)
    model = Model(inputs=input_model.input, outputs=x)

    image = model.input
    loss = K.sum(model.output)
    conv_output = [l for l in model.layers if l.name == layer_name][0].output
    grads = normalize(_compute_gradients(loss, [conv_output])[0])

    return image, category_index, conv_output, grads

def grad_cam(output, grads_val):
    output, grads_val = output[0, :], grads_val[0, :, :, :]

    weights = np.mean(grads_val, axis = (0, 1))
    cam = np.ones(output.shape[0 : 2], dtype = np.float32)

    for i, w in enumerate(weights):
        cam += w * output[:, :, i]

    cam = cv2.resize(cam, (32, 32))
    cam = np.maximum(cam, 0)
    heatmap = cam / (np.max(cam) + 1e-4)

    return heatmap


def run_experiment(m, n, x, y, modes, path, iter, output_folder, sim_fun):
    pname = path.split('/')[-1]
    population = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    no_mat = (len(modes) == 1) & (modes[0] == 'random')

    start_time = time.time()
    models = [keras.models.load_model('{}/{}'.format(path, k)) for k in population]
    print('{:d} models loaded in {:d} seconds.'.format(len(population), int(time.time()-start_time)))

    start_time = time.time()
    probs = [model.predict(x) for model in models]
    print('{:d} models predicted in {:d} seconds.'.format(len(population), int(time.time()-start_time)))

    start_time = time.time()
    mat = get_similarity_matrix(population, models, probs, x, sim_fun)
    res = []
    print('Similarity matrix for {:d} models created in {:d} seconds.'.format(len(population), int(time.time()-start_time)))

    start_time = time.time()
    res = {k:[] for k in modes}
    for i in range(iter):
        pool = [population[id] for id in random.sample(list(np.arange(0, len(population), 1)), m)]
        for mode in modes:
            mnames, score = generate_ensemble(pool, n, mode, mat)

            idx = [id for id, m in enumerate(pool) if m in mnames]
            _, epreds = ensemble_predict([probs[id] for id in idx], 'mv')
            acc = np.mean(np.equal(epreds, [label[0] for label in y]))
            res[mode].append((acc, score))

    for k, v in res.items():
        fname = k +'_' + pname + '_' + time.strftime("%Y%m%d-%H%M%S")
        np.save('{}/{}'.format(output_folder, fname), v)


    print('Experiment done in {:d} seconds ({:d} iterations and {:d} models per ensemble)'.format(int(time.time()-start_time), iter, n))

    return res


# uses a set of model names (ensemble) to predict
def ensemble_predict(probs, mode='mv'):
    preds = [list(np.argmax(p, axis=1)) for p in probs]

    if mode == 'mv': # majority voting        
        def get_majority(votes):
            majority = None
            majority_count = 0

            for vote in votes:
                if majority_count == 0:
                    majority = vote

                if vote == majority:
                    majority_count += 1
                else:
                    majority_count -= 1

            return majority
        
        res = [get_majority(votes) for votes in np.transpose(preds)]

    return preds, res


# returns a set of model names for the ensemble
def generate_ensemble(pool, n, mode='random', mat=None, k=1000):
    if mode == 'random':
        idx = random.sample(list(np.arange(0, len(pool), 1)), n)
        mnames = [pool[i] for i in idx]
        score = get_ensemble_similarity(idx, mat)

    if mode == 'max':
        mnames = []
        score = 2
        for j in range(k):
            idx = random.sample(list(np.arange(0, len(pool), 1)), n)
            temp = get_ensemble_similarity(idx, mat)
            if temp < score:
                score = temp
                mnames = [pool[i] for i in idx]

    if mode == 'min':
        mnames = []
        score = -2
        for j in range(k):
            idx = random.sample(list(np.arange(0, len(pool), 1)), n)
            temp = get_ensemble_similarity(idx, mat)
            if temp > score:
                score = temp
                mnames = [pool[i] for i in idx]

    return mnames, score


def get_similarity_matrix(pool, models, probs, x, sim_fun):
    hmaps = {k:[] for k in pool}
    layers = [[layer.name for layer in model.layers if 'conv2d' in layer.name][-1] for model in models]
    preds = [list(np.argmax(p, axis=1)) for p in probs]

    # get heatmaps
    start_time = time.time()
    for mname, model, layer, labels in zip(pool, models, layers, preds):
        image, category_index, conv_output, grads = get_gradient(model, None, None, layer)
        for i, y in enumerate(labels):
            if i%5000 == 0: print(i)
            output, grads_val = sess.run([conv_output, grads], feed_dict={image: x[i][np.newaxis, ...], category_index:y})
            hmaps[mname].append(grad_cam(output, grads_val))
    print('Heatmap creation for {:d} models took {:d} seconds'.format(len(pool), int(time.time() - start_time)))

    # create matrix
    mat = np.empty(shape=(len(pool), len(pool)))
    mat.fill(None)
    
    for i, row in enumerate(pool):
        for j, col in enumerate(pool):
            if (i != j) & (np.isnan(mat[j][i])):
                temp = []
                for k, _ in enumerate(hmaps[row]): temp.append(sim_fun(hmaps[row][k], hmaps[col][k]))
                mat[i][j] = np.mean(temp)

    return mat


def get_ensemble_similarity(idx, mat):
    res = []
    for i in idx:
        for j in idx:
            if not np.isnan(mat[i][j]): res.append(mat[i][j])

    return np.mean(res)


def compare_models(mnames, x_test, y_test):
    models = [keras.models.load_model('{}/{}.h5'.format(DIR, k)) for k in mnames]
    layers = [[layer.name for layer in model.layers if 'conv2d' in layer.name][-1] for model in models]
    preds = {k: [] for k in mnames}
    scores, hmaps = [], []
    labels = [y[0] for y in y_test]

    # Get indices of correct prediction by both models
    for x, y in zip(x_test, y_test):
        for i, mname in enumerate(mnames):
            preds[mname].append(np.argmax(models[i].predict(x[np.newaxis, ...])))
    corr = [list(np.equal(labels, preds[i])) for i in mnames]
    idx = np.where(corr[0] and corr[1])[0]
    print('{} correct predictions.'.format(str(len(idx))))

    # Get heatmaps for each model
    for model, layer in zip(models, layers):
        image, category_index, conv_output, grads = get_gradient(model, None, None, layer)
        temp = []
        for i, id in enumerate(idx):
            if i%2000 == 0: print(i)
            output, grads_val = sess.run([conv_output, grads], feed_dict={image: x_test[id][np.newaxis, ...], category_index:y_test[id][0]})
            temp.append(grad_cam(output, grads_val))
        hmaps.append(temp)

    # Compare heatmaps
    for i in range(len(idx)):
        corrcoeff = cc(hmaps[0][i], hmaps[1][i])
        kld = kldiv(hmaps[0][i], hmaps[1][i])
        scores.append([corrcoeff, kld])

    scores = np.array(scores)
    nan_idx = np.isnan(scores)
    scores[nan_idx] = 0

    res = {'preds': preds, 'acc': {k: np.mean(corr[i]) for i, k in enumerate(mnames)}, 'scores': scores}
    with open('../results/comparisons/{}_v_{}.pic'.format(mnames[0], mnames[1]), 'wb') as f: pickle.dump(res, f)


def get_heatmaps(mname, x, y, path):
    model = keras.models.load_model('{}/{}'.format(path, mname))
    layer = [layer.name for layer in model.layers if 'conv2d' in layer.name][-1]

    res = []

    image, category_index, conv_output, grads = get_gradient(model, None, None, layer)
    for i, (img, lab) in enumerate(zip(x, y)):
        if i % 1000 == 0: print(i)
        output, grads_val = sess.run([conv_output, grads], feed_dict={image: img[np.newaxis, ...], category_index:lab[0]})
        res.append(grad_cam(output, grads_val))

    return res


def plot_comparison(mnames, x_test, y_test, n):
    models = [keras.models.load_model('{}/{}.h5'.format(DIR, k)) for k in mnames]
    layers = [[layer.name for layer in model.layers if 'conv2d' in layer.name][-1] for model in models]
    images, hmaps, scores, indices = [], [], [], []
    count = 0

    image_0, category_index_0, conv_output_0, grads_0 = get_gradient(models[0], None, None, layers[0])
    image_1, category_index_1, conv_output_1, grads_1 = get_gradient(models[1], None, None, layers[1])
    for i, (x, y) in enumerate(zip(x_test, y_test)):
        if count < n:
            pred_0 = np.argmax(models[0].predict(x[np.newaxis, ...]))
            pred_1 = np.argmax(models[1].predict(x[np.newaxis, ...]))
            if (pred_0 == y[0]) & (pred_1 == y[0]):
                output_0, grads_val_0 = sess.run([conv_output_0, grads_0], feed_dict={image_0: x[np.newaxis, ...], category_index_0: y[0]})
                output_1, grads_val_1 = sess.run([conv_output_1, grads_1], feed_dict={image_1: x[np.newaxis, ...], category_index_1: y[0]})
                hmap = [grad_cam(output_0, grads_val_0), grad_cam(output_1, grads_val_1)]
                corr = cc(hmap[0], hmap[1])

                images.append((x, y))
                hmaps.append(hmap)
                scores.append([corr])
                indices.append(i)

                count += 1

    sorted_cc = np.argsort([s[0] for s in scores])[::-1]

    fig, axes = plt.subplots(10, np.maximum(1, int(n/10)), figsize=(14, 5), squeeze=False)

    for i, idx in enumerate(sorted_cc):
        col = int(np.floor(i/10))
        row = i-(10*col) if i+1 > 10 else i

        axes[row][col].imshow(np.append(images[idx][0], images[idx][0], axis=1))
        axes[row][col].imshow(np.append(hmaps[idx][0], hmaps[idx][1], axis=1), cmap="jet", alpha=0.5)
        axes[row][col].text(65, 8, str(indices[idx]))
        axes[row][col].text(65, 24, str(np.round(scores[idx][0], 3)))


    plt.setp(axes, xticks=[], yticks=[])
    plt.tight_layout(h_pad=0.2)
    plt.savefig('../results/plots/{}_v_{}.png'.format(mnames[0], mnames[1]))


def test_experiment(x, y, modes, path, sim_fun):
    pname = path.split('/')[-1]
    population = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))][:5]
    no_mat = (len(modes) == 1) & (modes[0] == 'random')

    start_time = time.time()
    models = [keras.models.load_model('{}/{}'.format(path, k)) for k in population]
    print('{:d} models loaded in {:d} seconds.'.format(len(population), int(time.time()-start_time)))

    start_time = time.time()
    probs = [model.predict(x) for model in models]
    print('{:d} models predicted in {:d} seconds.'.format(len(population), int(time.time()-start_time)))

    start_time = time.time()
    mat = get_similarity_matrix(population, models, probs, x, sim_fun)
    res = []
    print('Similarity matrix for {:d} models created in {:d} seconds.'.format(len(population), int(time.time()-start_time)))

    start_time = time.time()
    res = {k:[] for k in modes}
    for i in range(10):
        pool = [population[id] for id in random.sample(list(np.arange(0, len(population), 1)), 4)]
        for mode in modes:
            mnames, score = generate_ensemble(pool, 3, mode, mat)

            idx = [id for id, m in enumerate(pool) if m in mnames]
            _, epreds = ensemble_predict([probs[id] for id in idx], 'mv')
            acc = np.mean(np.equal(epreds, [label[0] for label in y]))
            res[mode].append((acc, score))


    print('Experiment done in {:d} seconds (10 iterations and 3 models per ensemble)'.format(int(time.time()-start_time)))
    
    for k, v in res.items():
        print(k)
        acc = [t[0] for t in v]
        sim = [t[1] for t in v]
        print('Accuracy - mean: {:5.3f}, std: {:5.3f}'.format(np.mean(acc), np.std(acc)))  
        print('Similarity - mean: {:5.3f}, std: {:5.3f}'.format(np.mean(sim), np.std(sim)))  

def test_method(x, y, modes, path, sim_fun):
    pname = path.split('/')[-1]
    mname = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))][0]

    start_time = time.time()
    model = keras.models.load_model('{}/{}'.format(path, mname))
    explainer = GradCam(model, layer=None)
    exp = explainer.explain(x[0], y[0][0])
    print(exp)

if __name__ == '__main__':
    pname = 'arch'
    path = '../model/' + pname

    (x_train, y_train), (x_test, y_test) = datasets.cifar10.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0

    random.seed(1313)
    modes = ['random', 'max', 'min']

    #test_method(x_test[:2000], y_test[:2000], modes, path, cc)
    test_experiment(x_test[:2000], y_test[:2000], modes, path, cc)
    #res = run_experiment(m=20, n=10, x=x_test, y=y_test, modes=modes, path=path, iter=10, output_folder='../results/ssim', sim_fun=ssim)

    """
    # search for number of networks in an ensemble
    res = {}
    random.seed(1313)
    for n in np.arange(3, 13, 1):
        res[str(n)] = []
        for _ in range(5):
            idx = random.sample(list(np.arange(0, len(pool), 1)), n)
            mnames = [pool[i] for i in idx]
            _, preds = ensemble_predict(mnames, x_test)
            res[str(n)].append(np.mean(np.equal(res, [y[0] for y in y_test])))

    with open('../results/arch_test.pic', 'wb') as f: pickle.dump(res, f)

    #to_compare = [['cnn4_a', 'cnn4_b'], ['cnn6_a', 'cnn6_b'], ['cnn4_a', 'cnn6_a'], ['cnn4_b', 'cnn6_b'], ['cnn4_a', 'cnn6_b'], ['cnn4_b', 'cnn6_a']]
    to_compare = [['cnn4_a', 'cnn4_b'], ['cnn6_a', 'cnn6_b'], ['cnn4_a', 'cnn6_a']]

    for mnames in to_compare:
        if os.path.exists('../results/comparisons/{}_v_{}.pic'.format(mnames[0], mnames[1])):
            with open('../results/comparisons/{}_v_{}.pic'.format(mnames[0], mnames[1]), 'rb') as f: res = pickle.load(f)
        else:
            compare_models(mnames, x_test, y_test)
            with open('../results/comparisons/{}_v_{}.pic'.format(mnames[0], mnames[1]), 'rb') as f: res = pickle.load(f)

        print(res['acc'])
        print('Min, Max, Mean, Standard Deviation')
        pcorr = pd.Series([s[0] for s in res['scores']]).agg(['min', 'max', 'mean', 'std'])
        kld = pd.Series([s[1] for s in res['scores']]).agg(['min', 'max', 'mean', 'std'])

        print("Pearson's correlation: {:5.3f}, {:5.3f}, {:5.3f}, {:5.3f}".format(pcorr[0], pcorr[1], pcorr[2], pcorr[3]))
        print("KL-divergence: {:5.3f}, {:5.3f}, {:5.3f}, {:5.3f}".format(kld[0], kld[1], kld[2], kld[3]))
    
    for mnames in to_compare:
        if not os.path.exists('../results/plots/{}_v_{}.png'.format(mnames[0], mnames[1])):
            plot_comparison(mnames, x_test, y_test, 50)
    #debug_heatmaps(mnames, x_test, y_test, [28, 18])

    #store_heatmaps('cnn3_5', x_test, y_test)
    #with open('../results/{}_hmaps.pic'.format('cnn3_5'), 'rb') as f: hmaps = pickle.load(f)
    """



