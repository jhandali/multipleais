# https://ermlab.com/en/blog/nlp/cifar-10-classification-using-keras-tutorial/

from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

from keras import models, layers

class cnn4():

    def __init__(self, conv2d_n=[64, 64], conv2d_size=[3, 3], dropout_rate=[0.25, 0.25, 0.5]):
        self.model = models.Sequential()
        self.model.add(layers.Conv2D(32, (3, 3), input_shape=(32, 32, 3)))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Conv2D(32, (3, 3)))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.MaxPooling2D((2, 2)))
        self.model.add(layers.Dropout(dropout_rate[0]))

        self.model.add(layers.Conv2D(conv2d_n[0], (conv2d_size[0], conv2d_size[0]), padding='same'))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Conv2D(conv2d_n[1], (conv2d_size[1], conv2d_size[1])))
        self.model.add(layers.MaxPooling2D((2, 2)))
        self.model.add(layers.Dropout(dropout_rate[1]))

        self.model.add(layers.Flatten())
        self.model.add(layers.Dense(512))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Dropout(dropout_rate[2]))
        self.model.add(layers.Dense(10))
        self.model.add(layers.Activation('softmax'))

class cnn6():

    def __init__(self, conv2d_n=[64, 64, 128, 128], conv2d_size=[3, 3, 3, 3], dropout_rate=[0.2, 0.2, 0.2, 0.2, 0.2]):
        self.model = models.Sequential()
        self.model.add(layers.Conv2D(32, (3, 3), input_shape=(32, 32, 3)))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Dropout(dropout_rate[0]))

        self.model.add(layers.Conv2D(32, (3, 3), padding='same'))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.MaxPooling2D((2, 2)))

        self.model.add(layers.Conv2D(conv2d_n[0], (conv2d_size[0], conv2d_size[0]), padding='same'))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Dropout(dropout_rate[1]))

        self.model.add(layers.Conv2D(conv2d_n[1], (conv2d_size[1], conv2d_size[1]), padding='same'))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.MaxPooling2D((2, 2)))

        self.model.add(layers.Conv2D(conv2d_n[2], (conv2d_size[2], conv2d_size[2]), padding='same'))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Dropout(dropout_rate[2]))

        self.model.add(layers.Conv2D(conv2d_n[3], (conv2d_size[3], conv2d_size[3]), padding='same'))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.MaxPooling2D((2, 2)))

        self.model.add(layers.Flatten())
        self.model.add(layers.Dropout(dropout_rate[3]))
        self.model.add(layers.Dense(1024))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Dropout(dropout_rate[4]))
        self.model.add(layers.Dense(10))
        self.model.add(layers.Activation('softmax'))