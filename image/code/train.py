from __future__ import absolute_import, division, print_function, unicode_literals

from tensorflow.keras import datasets
import keras
import time
import keras.backend as K
import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
import time

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.7
sess = tf.Session(config=config)
K.set_session(sess)

from models import cnn4, cnn6

DIR = '../model'
batch_size = 32
num_classes = 10

(train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()
# Normalize pixel values to be between 0 and 1
train_images, test_images = train_images / 255.0, test_images / 255.0
# Convert class vectors to binary class matrices.
train_labels = keras.utils.to_categorical(train_labels, num_classes)
test_labels = keras.utils.to_categorical(test_labels, num_classes)

class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

# Model

# cnn6 architecture variations
epochs = 50

for n in [[64, 64, 64, 128], [64, 64, 128, 512], [64, 64, 128, 128]]:
    for size in [[3, 3, 3, 5], [3, 3, 5, 5], [3, 3, 3, 3]]:
        for rate in [[0.1, 0.1, 0.1, 0.2, 0.2], [0.2, 0.2, 0.2, 0.2, 0.2]]:
            keras.backend.clear_session()
            mname = 'cnn6_n' + ''.join([str(i) for i in n]) + '_s' + ''.join([str(i) for i in size]) + '_r' + ''.join([str(int(i*10)) for i in rate])
            model = cnn6(**{'conv2d_n':n, 'conv2d_size':size, 'dropout_rate':rate}).model
            model.compile(optimizer='adam',
                          loss='categorical_crossentropy',
                          metrics=['accuracy'])

            start_time = time.time()

            history = model.fit(train_images, train_labels, epochs=epochs, validation_data=(test_images, test_labels), verbose=0)
            print('Training time for model {} with {:d} epochs: {:5.3f} seconds'.format(mname, epochs, time.time() - start_time))

            test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=0)

            print('Test loss: {:5.3f}, accuracy: {:5.3f}'.format(test_loss, test_acc))
            print('Train loss: {:5.3f}, accuracy: {:5.3f}'.format(history.history['loss'][-1], history.history['acc'][-1]))

            model.save('{}/arch/{}.h5'.format(DIR, mname))

"""
# cnn6 intialization variation (same architecture)
epochs = 20

for i in range(30):
    keras.backend.clear_session()
    mname = 'cnn6_' + time.strftime("%Y%m%d-%H%M%S")
    model = cnn6(**{'conv2d_n':[64, 64, 128, 128], 'conv2d_size':[3, 3, 3, 3], 'dropout_rate':[0.2, 0.2, 0.2, 0.2, 0.2]}).model
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    start_time = time.time()

    history = model.fit(train_images, train_labels, epochs=epochs, validation_data=(test_images, test_labels), verbose=0)
    print('Training time for model {} with {:d} epochs: {:5.3f} seconds'.format(mname, epochs, time.time() - start_time))

    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=0)

    print('Test loss: {:5.3f}, accuracy: {:5.3f}'.format(test_loss, test_acc))
    print('Train loss: {:5.3f}, accuracy: {:5.3f}'.format(history.history['loss'][-1], history.history['acc'][-1]))

    model.save('{}/init_cnn6/{}.h5'.format(DIR, mname))

# cnn4 intialization variation (same architecture)
epochs = 20

for i in range(30):
    keras.backend.clear_session()
    mname = 'cnn4_' + time.strftime("%Y%m%d-%H%M%S")
    model = cnn4(**{'conv2d_n':[64, 64], 'conv2d_size':[3, 3], 'dropout_rate':[0.25, 0.25, 0.5]}).model
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    start_time = time.time()

    history = model.fit(train_images, train_labels, epochs=epochs, validation_data=(test_images, test_labels), verbose=0)
    print('Training time for model {} with {:d} epochs: {:5.3f} seconds'.format(mname, epochs, time.time() - start_time))

    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=0)

    print('Test loss: {:5.3f}, accuracy: {:5.3f}'.format(test_loss, test_acc))
    print('Train loss: {:5.3f}, accuracy: {:5.3f}'.format(history.history['loss'][-1], history.history['acc'][-1]))

    model.save('{}/init_cnn4/{}.h5'.format(DIR, mname))


# cnn4 architecture variations
epochs = 50

for n in [[32, 64], [64, 128], [64, 64]]:
    for size in [[3, 5], [5, 5], [3, 3]]:
        for rate in [[0.1, 0.1, 0.5], [0.25, 0.25, 0.5]]:
            keras.backend.clear_session()
            mname = 'cnn4_n' + ''.join([str(i) for i in n]) + '_s' + ''.join([str(i) for i in size]) + '_r' + ''.join([str(int(i*10)) for i in rate])
            model = cnn4(**{'conv2d_n':n, 'conv2d_size':size, 'dropout_rate':rate}).model
            model.compile(optimizer='adam',
                          loss='categorical_crossentropy',
                          metrics=['accuracy'])

            start_time = time.time()

            history = model.fit(train_images, train_labels, epochs=epochs, validation_data=(test_images, test_labels), verbose=0)
            print('Training time for model {} with {:d} epochs: {:5.3f} seconds'.format(mname, epochs, time.time() - start_time))

            test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=0)

            print('Test loss: {:5.3f}, accuracy: {:5.3f}'.format(test_loss, test_acc))
            print('Train loss: {:5.3f}, accuracy: {:5.3f}'.format(history.history['loss'][-1], history.history['acc'][-1]))

            model.save('{}/arch/{}.h5'.format(DIR, mname))

for epochs, mname in zip([5], ['cnn4_x']):
    keras.backend.clear_session()
    model = cnn4(**cnn4_params).model
    #model = cnn6().model

    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    start_time = time.time()

    history = model.fit(train_images, train_labels, epochs=epochs, validation_data=(test_images, test_labels), verbose=1)
    print('Training time for model {} with {:d} epochs: {:5.3f} seconds'.format(mname, epochs, time.time() - start_time))

    test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=0)

    print(history.history['loss'])
    print('Test loss: {:5.3f}, accuracy: {:5.3f}'.format(test_loss, test_acc))
    print('Train loss: {:5.3f}, accuracy: {:5.3f}'.format(history.history['loss'][-1], history.history['acc'][-1]))

    model.save('{}/{}.h5'.format(DIR, mname))
"""