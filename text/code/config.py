class cnnText:
    def __init__(self, isDummy = True):
        self.max_features = 5000
        self.maxlen = 400
        self.batch_size = 32
        self.embedding_dims = 50
        self.hidden_dims = 250
        self.epochs = 2

