from __future__ import print_function

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from keras.datasets import imdb
import numpy as np

from config import cnnText as cfg
from models import cnnText

Conf = cfg()

if __name__ == '__main__':

    print('Loading data...')
    np_load_old = np.load

    # modify the default parameters of np.load
    np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)

    (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=Conf.max_features)

    # restore np.load for future normal usage
    np.load = np_load_old

    print(len(x_train), 'train sequences')
    print(len(x_test), 'test sequences')

    print('Pad sequences (samples x time)')
    x_train = sequence.pad_sequences(x_train, maxlen=Conf.maxlen)
    x_test = sequence.pad_sequences(x_test, maxlen=Conf.maxlen)
    print('x_train shape:', x_train.shape)
    print('x_test shape:', x_test.shape)

    print('Build model...')
    model = cnnText()

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    model.fit(x_train, y_train,
              batch_size=Conf.batch_size,
              epochs=Conf.epochs,
              validation_data=(x_test, y_test))