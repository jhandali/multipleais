from __future__ import print_function

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from keras.datasets import imdb

from config import cnnText as cfg
Conf = cfg()

def cnnText(n_conv=1, kernel_size=[3], filters=[250]):
    
    model = Sequential()

    model.add(Embedding(Conf.max_features, Conf.embedding_dims, input_length=Conf.maxlen))
    model.add(Dropout(0.2))

    # we add a Convolution1D, which will learn filters
    # word group filters of size filter_length:
    for i in range(n_conv):
        model.add(Conv1D(filters[i],
                         kernel_size[i],
                         padding='valid',
                         activation='relu',
                         strides=1))
    # we use max pooling:
    model.add(GlobalMaxPooling1D())

    # We add a vanilla hidden layer:
    model.add(Dense(Conf.hidden_dims))
    model.add(Dropout(0.2))
    model.add(Activation('relu'))

    # We project onto a single unit output layer, and squash it with a sigmoid:
    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    return model